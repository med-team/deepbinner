Source: deepbinner
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3,
               python3-setuptools,
               python3-keras,
               python3-h5py,
               python3-numpy,
               python3-edlib,
               python3-mappy,
               python3-noise,
#               python3-tensorflow
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/deepbinner
Vcs-Git: https://salsa.debian.org/med-team/deepbinner.git
Homepage: https://github.com/rrwick/Deepbinner
Rules-Requires-Root: no

Package: deepbinner
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: demultiplexing barcoded Oxford Nanopore sequencing reads
 Deepbinner is a tool for demultiplexing barcoded Oxford Nanopore
 sequencing reads. It does this with a deep convolutional neural network
 classifier, using many of the architectural advances that have proven
 successful in image classification. Unlike other demultiplexers (e.g.
 Albacore and Porechop), Deepbinner identifies barcodes from the raw
 signal (a.k.a. squiggle) which gives it greater sensitivity and fewer
 unclassified reads.
 .
 Reasons to use Deepbinner:
  * To minimise the number of unclassified reads (use Deepbinner
    by itself).
  * To minimise the number of misclassified reads (use Deepbinner in
    conjunction with Albacore demultiplexing).
  * You plan on running signal-level downstream analyses, like
    Nanopolish. Deepbinner can demultiplex the fast5 files which makes
    this easier. Reasons to not use Deepbinner:
  * You only have basecalled reads not the raw fast5 files (which
    Deepbinner requires).
  * You have a small/slow computer. Deepbinner is more computationally
    intensive than Porechop.
  * You used a sequencing/barcoding kit other than the ones Deepbinner
    was trained on.
